#!python3
# Extrai o código de máquina (compilado pelo gcc) da função fatorial para um
# arquivo de texto

START = 0x5fa # Começo da função fatorial segundo o gdb
END   = 0x625 # Fim    da função fatorial segundo o gdb

with open("exemplo.bin", "rb"), as machine_code:
    bs = machine_code.read()[START:END]

    with open("bin.txt", "w", encoding="utf8") as output:
        output.write(" ".join([f"{byte:b}".zfill(8) for byte in bs]))

